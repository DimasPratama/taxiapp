

import React, {Component} from 'react';
import {Platform,Button, StyleSheet, Text, View,TextInput,TouchableOpacity,ActivityIndicator} from 'react-native';
import MapView ,{Polyline, Marker} from "react-native-maps";

import _ from "lodash"
import PolyLine from"@mapbox/polyline"
import socketIO from "socket.io-client"


export default class App extends Component{
    constructor(props){
        super(props)
        this.state={

            error:"",
            latitude:0,
            longitude:0,
            destination:'',
            predictions:[],
            pointCoords:[],
            isDriver: false,
            isPassenger: false,
            lookingForPassenger: false
        }
        this.onChangeDestinationDebounced = _.debounce(
            this.onChangeDestination,
            1000
        )
    }

    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({
                    latitude:position.coords.latitude,
                    longitude:position.coords.longitude
                })
                // this.getRouteDirections();
            },
            error => this.setState({error:error.message}),
            {enableHighAccuracy:true,maximumAge:2000, timeout:20000}
        )
    }

    async getRouteDirections(placeId,destinationName){
        try{
            const response = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.latitude},${this.state.longitude}&destination=place_id:${placeId}&key=AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw`)
            const  json = await response.json()
            // console.log(json)
            const points=PolyLine.decode(json.routes[0].overview_polyline.points)
            const pointCoords = points.map(point =>
                {
                    return{latitude:point[0],longitude:point[1]}
                }
            )
            this.setState({pointCoords, predictions:[],destinationName})

            this.map.fitToCoordinates(pointCoords,{
                edgePadding:{top:30,bottom:30,left:30,right:30}
            })
        }
        catch (error) {
            console.error(error)

        }

    }


    async onChangeDestination (destination){
        const apikey="AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw"
        const apiUrl=`https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apikey}&input=${destination}&location=${this.state.latitude}, ${this.state.longitude}&radius=2000`

        try{
            const result = await fetch(apiUrl)
            const json = await result.json()
            console.log(json)
            this.setState({predictions:json.predictions})
        }catch(e){
            console.log(e)
        }
    }

    async lookForPassenger(){
        this.setState({
            lookingForPassengers: true
        });

        const socket = socketIO.connect("http://10.10.103.113:3000");

        socket.on("connect",()=>{
            socket.emit("lookingForPassenger");
        });

        socket.on("taxiRequest",routeResponse=>{
            console.log(routeResponse);
        })



    }

    render() {
        let marker = null
        if(this.state.pointCoords.length >1){
            marker=(
                <Marker coordinate={this.state.pointCoords[this.state.pointCoords.length-1]} />
            )

        }
        return (
            <View style={styles.container}>


                <MapView
                    ref={map =>
                        this.map = map
                    }

                    style={styles.map}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0200,
                    }}
                    showsUserLocation={true}
                >
                    <Polyline
                        coordinates={this.state.pointCoords}
                        strokeWidth={2}
                        strokeColor={"black"}
                    />
                    {marker}
                </MapView>
                <TouchableOpacity
                onPress={() => this.lookForPassenger()}
                style={styles.bottomButton}
                >
                    <View>
                        <Text style={styles.bottomButtonText}>FIND PASSENGER</Text>
                        {this.state.lookingForPassengers === true ?(
                            <ActivityIndicator
                                animating={this.state.lookingForPassengers}
                                size="large"
                            />

                        ):null}
                    </View>
                </TouchableOpacity>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    suggestion:{
        backgroundColor: "white",
        padding: 5,
        fontSize:18,
        borderWidth: 0.5,
        marginLeft: 5,
        marginRight: 5
    },
    container: {
        ...StyleSheet.absoluteFillObject,
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    destination:{
        height:40,
        borderWidth:0.5,
        marginRight:5,
        marginLeft:5,
        marginTop:50,
        padding:5,
        backgroundColor:"white"

    }
});