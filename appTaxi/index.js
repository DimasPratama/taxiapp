/**
 * @format
 */

import {AppRegistry} from 'react-native';
import HomeChoose from './screens/homeChoose';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () =>HomeChoose);
